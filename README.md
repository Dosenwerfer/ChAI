# ChAI

This project aims to be a basic AI for chess using a feed-forward neural network and backpropagation as a primary training method using the [FICS Games database](https://www.ficsgames.org/) as training data.

It works by

1. evaluating the current board with all rule relevant past moves,
2. enumerating the legal moves in that position,
3. evaluating each move by feeding the current position as well as the position after the move as inputs for the neural network which outputs a scalar value of how good it thinks this move is,
4. selecting best move.

## Authors

* **Dosenwerfer** - [Dosenwerfer](https://gitlab.com/Dosenwerfer)

## Acknowledgments

* [Icon Source](https://www.pexels.com/photo/black-white-and-brown-chess-board-game-139392/)
