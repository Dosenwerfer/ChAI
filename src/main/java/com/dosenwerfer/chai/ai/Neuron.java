package com.dosenwerfer.chai.ai;

import lombok.Getter;

/**
 * @author Dosenwerfer (24-Oct-18).
 */
public class Neuron {

    @Getter
    private final Layer layer;
    @Getter
    private final int previousNeurons;
    @Getter
    private double[] weights;
    @Getter
    private double bias;

    /**
     * @param layer Layer the neuron belongs to
     * @param previousNeurons Number of neurons in previous layer
     */
    public Neuron( Layer layer, int previousNeurons ) {
        this.layer = layer;
        this.previousNeurons = previousNeurons;
        weights = new double[previousNeurons];
        bias = 0;
    }

    public double activate( double[] inputs ) throws IllegalArgumentException {
        if ( inputs.length != previousNeurons ) {
            throw new IllegalArgumentException( "Number of inputs doesn't match this neuron's weight count!" );
        }
        double activation = bias;
        for ( int i = 0; i < previousNeurons; ++i ) {
            activation += weights[i] * inputs[i];
        }
        return sigmoid( activation );
    }

    private double sigmoid( double input ) {
        return 1 / (1 + Math.exp( -input ));
    }
}
