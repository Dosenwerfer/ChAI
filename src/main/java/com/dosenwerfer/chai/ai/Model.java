package com.dosenwerfer.chai.ai;

/**
 * TODO split evaluation into two steps: preperation (always same multiplications with "current position"-inputs and first hidden layer) and actual new calculations
 * @author Dosenwerfer (05-Sep-18).
 */
public class Model {

    // (8^2 Fields * 6 PieceTypes * 2 Colors + 2 * 1 players ability to castle) * 2 for current position and position after move = 1540 Inputs
    private final int inputNeurons = (8*8 * 6 * 2 + 2) * 2;
    private Layer[] layers;

    public Model() {
        layers = new Layer[3];
        layers[0] = new Layer( this, 16 );
        layers[1] = new Layer( this, 16 );
        layers[2] = new Layer( this, 1 );
    }

    public double[] evaluate( double[] input ) throws IllegalArgumentException {
        if ( input.length != inputNeurons ) {
            throw new IllegalArgumentException( "Number of input neurons doesn't match defined input neurons!" );
        }
        double[] activations = input;
        for ( int i = layers.length - 1; i >= 0; --i ) {
            activations = layers[i].evaluate( activations );
        }
        return activations;
    }
}
