package com.dosenwerfer.chai.ai;

import lombok.Getter;

/**
 * TODO increase efficiency: make direct matrix-vector multiplication instead of having individual neuron objects
 * @author Dosenwerfer (24-Oct-18).
 */
public class Layer {

    @Getter
    private final Model model;
    @Getter
    private final int n;
    private Neuron[] neurons;

    /**
     * @param model Model this layer belongs to
     * @param n Number of neurons in this layer
     */
    public Layer( Model model, int n ) {
        this.model = model;
        this.n = n;
        neurons = new Neuron[n];
    }

    public double[] evaluate( double[] inputs ) {
        double[] activations = new double[n];
        for ( int i = 0; i < n; ++i ) {
            activations[i] = neurons[i].activate( inputs );
        }
        return activations;
    }
}
