package com.dosenwerfer.chai;

import com.dosenwerfer.chai.chess.Game;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * TODO write parser for international chess notation
 *
 * @author Dosenwerfer (05-Sep-18).
 */
public class Main {

    public static void main( String... args ) {
        System.out.println( "Power-up complete.");
        System.out.println( "Welcome to ChAI!" );
        System.out.println( "Oh, it's you..." ); // CAAAAKE
        System.out.println();

        try ( BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) ) ) {
            while ( true ) {
                System.out.println( "- Main menu -" );
                System.out.println( "1: Start a game" );
                System.out.println( "2: Show current neural net" );
                System.out.println( "3: Train neural net" );
                System.out.println( "q: Quit" );
                System.out.print( ">" );
                String input = reader.readLine();
                // Catch control inputs and default to move evaluation
                switch ( input ) {
                    // Quit
                    case "quit":
                    case "q":
                        return;
                    // Start new game
                    case "1":
                        Game game = new Game();
                        game.start( reader );
                        break;
                    // Show current neural net
                    case "2":
                        break;
                    // Train neural net
                    case "3":
                        break;
                }
            }
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }
}
