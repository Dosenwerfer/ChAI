package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.pieces.Piece;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
@EqualsAndHashCode
public abstract class Move {

    @Getter
    protected final PieceColor color;

    protected Move( PieceColor color ) {
        this.color = color;
    }

    /**
     * Must clone the current position and apply this moves changes to it.
     *
     * @return Next position with this move's changes or <code>null</code> if the game has ended
     */
    public abstract Position apply( Position position );

    /**
     * Moves the piece from the specified square to a target square.
     *
     * @param board Board to operate on
     * @param from Origin square
     * @param to Destination Square
     */
    protected void movePieceInternal( Board board, Square from, Square to ) {
        // Get piece
        Piece piece = board.getPiece( from );

        // Check if destination is occupied and capture if so
        Piece captured = board.getPiece( to );
        if ( captured != null ) {
            captured.setSquare( null );
        }

        // Move piece to destination
        board.setPiece( to, piece );

        // Remove piece from source
        board.setPiece( from, null );

        // Flag piece as moved
        if ( piece != null ) {
            piece.setMoved( true );
        }
    }
}
