package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
@EqualsAndHashCode(callSuper = true)
public class TranslationMove extends Move {

    @Getter
    protected final Square from, to;

    public TranslationMove( PieceColor color, Square from, Square to ) {
        super( color );
        this.from = from;
        this.to = to;
    }

    public Position apply( Position position ) {
        Position nextPosition = position.getNextClone();

        // Move piece on board
        movePieceInternal( nextPosition.getBoard(), from, to );

        return nextPosition;
    }
}
