package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import lombok.Getter;

/**
 * @author Dosenwerfer (22-Jan-19).
 */
public class OutcomeMove extends Move {

    /**
     * Either draw or color that has won the game.
     */
    public enum Outcome {
        WHITE, BLACK, DRAW
    }

    /**
     * TODO
     */
    public enum DrawReason {
        THREEFOLD, STALEMATE, FIFTY // TODO
    }

    @Getter
    private Outcome outcome;

    public OutcomeMove( Outcome outcome, PieceColor player ) {
        super( player );
        this.outcome = outcome;
    }

    public Position apply( Position position ) {
        return null;
    }
}
