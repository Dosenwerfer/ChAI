package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import com.dosenwerfer.chai.chess.pieces.RookPiece;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Dosenwerfer (06-Sep-18).
 */
@EqualsAndHashCode( callSuper = true )
public class CastlingMove extends Move {

    public enum Direction {
        FROM, TO
    }

    public enum CastlingType {
        LONG( RookPiece.RookType.LONG ),
        SHORT( RookPiece.RookType.SHORT );

        @Getter
        private final RookPiece.RookType rookType;

        CastlingType( RookPiece.RookType rookType ) {
            this.rookType = rookType;
        }
    }

    /**
     * Returns castling square by color, type, piece and direction.
     *
     * @param color Color of castling
     * @param castlingType Long or short castling
     * @param rookType Involved rook or <code>null</code> for king
     * @param direction Origin or destination square
     * @return Square
     */
    public static Square getSquare( PieceColor color, CastlingType castlingType, RookPiece.RookType rookType, Direction direction ) {
        int row = color == PieceColor.WHITE ? 0 : 7;
        switch ( castlingType ) {
            case LONG:
                if ( rookType == null ) {
                    return direction == Direction.FROM ? new Square( row, 4 ) : new Square( row, 2 );
                } else if ( rookType == RookPiece.RookType.LONG ) {
                    return direction == Direction.FROM ? new Square( row, 0 ) : new Square( row, 3 );
                } else {
                    throw new IllegalArgumentException( "Illegal argument combination. Short rook doesn't participate in long castling!" );
                }
            case SHORT:
                if ( rookType == null ) {
                    return direction == Direction.FROM ? new Square( row, 4 ) : new Square( row, 6 );
                } else if ( rookType == RookPiece.RookType.SHORT ) {
                    return direction == Direction.FROM ? new Square( row, 7 ) : new Square( row, 5 );
                } else {
                    throw new IllegalArgumentException( "Illegal argument combination. Long rook doesn't participate in short castling!" );
                }
        }
        throw new IllegalArgumentException( "Illegal argument combination!" );
    }

    @Getter
    private final CastlingType type;

    public CastlingMove( PieceColor player, CastlingType type ) {
        super( player );
        this.type = type;
    }

    public Position apply( Position position ) {
        Position nextPosition = position.getNextClone();
        Board board = nextPosition.getBoard();

        // Move king
        movePieceInternal( board,
                getSquare( color, type, null, Direction.FROM ),
                getSquare( color, type, null, Direction.TO ) );
        // Move rook
        movePieceInternal( board,
                getSquare( color, type, type.getRookType(), Direction.FROM ),
                getSquare( color, type, type.getRookType(), Direction.TO ) );

        return nextPosition;
    }
}
