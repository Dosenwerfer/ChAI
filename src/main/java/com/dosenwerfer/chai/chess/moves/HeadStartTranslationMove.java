package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.pieces.PawnPiece;
import com.dosenwerfer.chai.chess.pieces.PieceColor;

/**
 * @author Dosenwerfer (22-Feb-19).
 */
public class HeadStartTranslationMove extends TranslationMove {

    public HeadStartTranslationMove( PieceColor player, Square from, Square to ) {
        super( player, from, to );
    }

    public Position apply( Position position ) {
        Position nextPosition = position.getNextClone();

        // Move piece on board
        movePieceInternal( nextPosition.getBoard(), from, to );
        // Flag pawn as head-started
        ((PawnPiece) nextPosition.getBoard().getPiece( to )).setJustHeadStarted( true );

        return nextPosition;
    }
}
