package com.dosenwerfer.chai.chess.moves;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.pieces.*;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (24-Oct-18).
 */
@EqualsAndHashCode(callSuper = true)
public class PromotionTranslationMove extends TranslationMove {

    private Piece promoted;

    public static HashSet<Move> addPromotionMoves( HashSet<Move> moves, Player player, Position position, Square from, Square to ) {
        Board board = position.getBoard();
        moves.add( new PromotionTranslationMove( player.getColor(), from, to, new QueenPiece( board, player, to ) ) );
        moves.add( new PromotionTranslationMove( player.getColor(), from, to, new KnightPiece( board, player, to ) ) );
        moves.add( new PromotionTranslationMove( player.getColor(), from, to, new RookPiece( board, player, to ) ) );
        moves.add( new PromotionTranslationMove( player.getColor(), from, to, new BishopPiece( board, player, to ) ) );

        return moves;
    }

    public static HashSet<Move> getPromotionMoves( Player player, Position position, Square from, Square to ) {
        return addPromotionMoves( new HashSet<>(), player, position, from, to );
    }

    public PromotionTranslationMove( PieceColor color, Square from, Square to, Piece promoted ) {
        super( color, from, to );
        this.promoted = promoted;
    }

    public Position apply( Position position ) {
        Position nextPosition = super.apply( position );

        // Add promoted piece to board
        nextPosition.getBoard().setPiece( to, promoted );

        return nextPosition;
    }
}
