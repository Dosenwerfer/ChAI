package com.dosenwerfer.chai.chess;

import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.moves.Move;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO think about PositionFactory
 * TODO implement 50-move-rule <.<
 * <p>
 * A Position represents a complete state of the game at a certain point in time.
 * This includes all board related information like pieces and their fields, as well
 * as past events that are relevant for the future like whose turn it is, how many times
 * this exact board constallation occured already (TODO threefold repetition) and if pawns
 * just did a head start (capture en passant).
 *
 * @author Dosenwerfer (05-Sep-18).
 */
@EqualsAndHashCode
public class Position implements Cloneable {

    @Getter
    private Board board;
    @Getter
    private HashMap<PieceColor, Player> players;
    @Getter
    private PieceColor turn; // Color of player to move

    @Getter @EqualsAndHashCode.Exclude
    private final transient Game game;
    @Getter @EqualsAndHashCode.Exclude
    private Position previousPosition;
    @Getter @EqualsAndHashCode.Exclude
    private transient Boolean threefoldRepetition;

    /**
     * Creates the default position.
     */
    public Position( Game game ) {
        // Save game reference
        this.game = game;

        // White's turn
        this.turn = PieceColor.WHITE;

        // Previous position
        this.previousPosition = null;

        // Create players (before board!)
        this.players = new HashMap<>();
        for ( PieceColor color : PieceColor.values() ) {
            this.players.put( color, new Player( color, this ) );
        }

        // Create default board
        this.board = new Board( this );

        // Initialize players (after board!)
        this.players.values().forEach( Player::computeMoves );

        // Initialize threefold repetition (after moves!)
        this.threefoldRepetition = computeThreefoldRepetition();
    }

    /**
     * Checks if threefold repetition has occured with this position
     * or will occur after any legal move of the color to move. If so
     * the player to move must be granted the right to claim a draw.
     * Since this part of this program's chess move model is rather
     * complicated, this draw move cannot be included in the legal moves
     * collection, so any move making decision needs to explicitly account
     * for this method.
     *
     * Threefold repetition is defined as the third time the same position
     * more explicitly the same set of legal moves has occured during a game.
     * The equal positions don't have to occur consecutively for this to apply.
     *
     * @return If threefold repetition has occured with this position
     * or will occur after any legal move of the color to move
     */
    private boolean computeThreefoldRepetition() {
        return game.getPositionOccurrences( this ) >= 2
                || players.get( turn ).getMoves().get( true ).stream()
                .anyMatch( move -> game.getPositionOccurrences( move.apply( this ) ) >= 2 );
    }

    /**
     * Clones this position, applies a move to it and returns the resulting position.
     *
     * @param move Move
     * @return Position after move
     */
    public Position applyMove( Move move ) {
        Position next = move.apply( this );

        if ( next != null ) {
            next.players.values().forEach( player -> {
                player.setPosition( next );

                // Compute new moves
                player.computeMoves();
            } );
        }

        return next;
    }

    /**
     * Intended for Move classes to get a prepared clone of this position.
     *
     * @return Prepared clone of this position
     */
    public Position getNextClone() {
        Position clone = this.clone();

        clone.previousPosition = this;
        clone.turn = this.turn.invert();
        clone.board.resetJustHeadStartedFlags();

        return clone;
    }

    /**
     * TODO position clone docs
     *
     * @return
     */
    @Override
    public Position clone() {
        Position clonedPosition = null;
        try {
            // Call super
            clonedPosition = (Position) super.clone();
            //// Handle deep copies
            // Copy players
            clonedPosition.players = new HashMap<>();
            for ( Map.Entry<PieceColor, Player> entry : this.players.entrySet() ) {
                PieceColor color = entry.getKey();
                Player player = entry.getValue();
                clonedPosition.players.put( color, player.clone() );
            }
            // Copy board (after players!)
            clonedPosition.board = this.board.clone();
        } catch ( CloneNotSupportedException e ) {
            e.printStackTrace();
        }
        return clonedPosition;
    }
}
