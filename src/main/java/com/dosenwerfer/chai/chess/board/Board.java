package com.dosenwerfer.chai.chess.board;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.pieces.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashSet;

/**
 * @author Dosenwerfer (22-Jan-19).
 */
@EqualsAndHashCode
public class Board implements Cloneable {

    private final Piece[][] pieces;

    @Getter @EqualsAndHashCode.Exclude
    private transient Position position;

    /**
     * Creates the default board state.
     */
    public Board( Position position ) {
        this.position = position;

        // Initiate data structures
        this.pieces = new Piece[8][8];

        // Get players
        Player white = position.getPlayers().get( PieceColor.WHITE );
        Player black = position.getPlayers().get( PieceColor.BLACK );

        // Create white pieces
        RookPiece whiteLongRook = new RookPiece( this, white, new Square( 0, 0 ) );
        white.getRooks().put( RookPiece.RookType.LONG, whiteLongRook );
        addPiece( whiteLongRook );
        addPiece( new KnightPiece( this, white, new Square( 0, 1 ) ) );
        addPiece( new BishopPiece( this, white, new Square( 0, 2 ) ) );
        addPiece( new QueenPiece( this, white, new Square( 0, 3 ) ) );
        KingPiece whiteKing = new KingPiece( this, white, new Square( 0, 4 ) );
        white.setKing( whiteKing );
        addPiece( whiteKing );
        addPiece( new BishopPiece( this, white, new Square( 0, 5 ) ) );
        addPiece( new KnightPiece( this, white, new Square( 0, 6 ) ) );
        RookPiece whiteShortRook = new RookPiece( this, white, new Square( 0, 7 ) );
        white.getRooks().put( RookPiece.RookType.SHORT, whiteShortRook );
        addPiece( whiteShortRook );

        // Create black pieces
        RookPiece blackLongRook = new RookPiece( this, black, new Square( 7, 0 ) );
        black.getRooks().put( RookPiece.RookType.LONG, blackLongRook );
        addPiece( blackLongRook );
        addPiece( new KnightPiece( this, black, new Square( 7, 1 ) ) );
        addPiece( new BishopPiece( this, black, new Square( 7, 2 ) ) );
        addPiece( new QueenPiece( this, black, new Square( 7, 3 ) ) );
        KingPiece blackKing = new KingPiece( this, black, new Square( 7, 4 ) );
        black.setKing( blackKing );
        addPiece( blackKing );
        addPiece( new BishopPiece( this, black, new Square( 7, 5 ) ) );
        addPiece( new KnightPiece( this, black, new Square( 7, 6 ) ) );
        RookPiece blackShortRook = new RookPiece( this, black, new Square( 7, 7 ) );
        black.getRooks().put( RookPiece.RookType.SHORT, blackShortRook );
        addPiece( blackShortRook );

        // Create pawns
        for ( byte i = 0; i < 8; ++i ) {
            pieces[1][i] = new PawnPiece( this, white, new Square( 1, i ) );
            pieces[6][i] = new PawnPiece( this, black, new Square( 6, i ) );
        }
    }

    public Piece getPiece( Square square ) {
        return pieces[square.getRow()][square.getColumn()];
    }

    public void setPiece( Square square, Piece piece ) {
        if ( square != null ) {
            pieces[square.getRow()][square.getColumn()] = piece;
        }
        if ( piece != null ) {
            piece.setSquare( square );
        }
    }

    public KingPiece getKing( PieceColor color ) {
        return position.getPlayers().get( color ).getKing();
    }

    public RookPiece getRook( PieceColor color, RookPiece.RookType type ) {
        return position.getPlayers().get( color ).getRooks().get( type );
    }

    public HashSet<Piece> getPieceSet() {
        HashSet<Piece> pieces = new HashSet<>();
        for ( int row = 0; row < 8; ++row ) {
            for ( int column = 0; column < 8; ++column ) {
                Piece piece = this.pieces[row][column];
                if ( piece == null ) {
                    continue;
                }
                pieces.add( piece );
            }
        }
        return pieces;
    }

    public HashSet<Piece> getPieceSet( PieceColor color ) {
        HashSet<Piece> pieces = new HashSet<>();
        for ( int row = 0; row < 8; ++row ) {
            for ( int column = 0; column < 8; ++column ) {
                Piece piece = this.pieces[row][column];
                if ( piece == null || piece.getColor() != color ) {
                    continue;
                }
                pieces.add( piece );
            }
        }
        return pieces;
    }

    public void resetJustHeadStartedFlags() {
        getPieceSet().forEach( piece -> {
            if ( piece instanceof PawnPiece ) {
                ((PawnPiece) piece).setJustHeadStarted( false );
            }
        } );
    }

    //region Display
    public void print() {
        System.out.println( getBoardString() );
    }

    public void printSymbols() {
        System.out.println( getBoardSymbols() );
    }

    public String getBoardString() {
        return getBoardRepresentation( false );
    }

    public String getBoardSymbols() {
        return getBoardRepresentation( true );
    }

    public boolean isOccupied( Square square ) {
        return getPiece( square ) != null;
    }

    public boolean isOccupied( Square square, PieceColor color ) {
        Piece piece = getPiece( square );
        return piece != null && piece.getColor() == color;
    }

    private String getBoardRepresentation( boolean symbolic ) {
        StringBuilder result = new StringBuilder();
        for ( int i = 7; i >= 0; --i ) {
            for ( int j = 0; j < 8; ++j ) {
                Piece piece = pieces[i][j];
                result.append( piece == null ? "  " : symbolic ? String.valueOf( piece.getSymbol() ) : piece.getToken() ).append( " " );
            }
            if ( i > 0 ) {
                result.append( System.lineSeparator() );
            }
        }
        return result.toString();
    }
    //endregion

    @Override
    public Board clone() {
        Board clonedBoard = null;
        try {
            // Call super
            clonedBoard = (Board) super.clone();
            // Handle deep copies
            // Clone pieces
            for ( int row = 0; row < 8; ++row ) {
                for ( int column = 0; column < 8; ++column ) {
                    if ( pieces[row][column] == null ) {
                        clonedBoard.pieces[row][column] = null;
                    } else {
                        Piece originalPiece = pieces[row][column];
                        Piece clonedPiece = originalPiece.clone();
                        clonedBoard.pieces[row][column] = clonedPiece;
                        Player clonePlayer = clonedPiece.getPlayer();

                        // Check if cloned piece was king or rook
                        if ( clonedPiece instanceof KingPiece ) {
                            // Update king reference for clone color
                            clonePlayer.setKing( (KingPiece) clonedPiece );
                        } else if ( clonedPiece instanceof RookPiece ) {
                            RookPiece clonedRook = (RookPiece) clonedPiece;
                            // Check if rook was short or long rook and update reference for clone color
                            if ( originalPiece == originalPiece.getPlayer().getRooks().get( RookPiece.RookType.SHORT ) ) {
                                clonePlayer.getRooks().put( RookPiece.RookType.SHORT, clonedRook );
                            } else if ( originalPiece == originalPiece.getPlayer().getRooks().get( RookPiece.RookType.LONG ) ) {
                                clonePlayer.getRooks().put( RookPiece.RookType.LONG, clonedRook );
                            }
                        }
                    }
                }
            }
        } catch ( CloneNotSupportedException e ) {
            e.printStackTrace();
        }
        return clonedBoard;
    }

    private void addPiece( Piece piece ) {
        this.pieces[piece.getSquare().getRow()][piece.getSquare().getColumn()] = piece;
    }
}
