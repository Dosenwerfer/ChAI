package com.dosenwerfer.chai.chess.board;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
@EqualsAndHashCode
public class Square {

    @Getter
    private final int row, column; // Range 0-7

    public Square( int row, int column ) {
        this.row = row;
        this.column = column;
    }

    public int getRank() {
        return row + 1;
    }

    public char getFile() {
        return (char) ('a' + column);
    }

    public boolean isInBounds() {
        return row >= 0 && row <= 7 && column >= 0 && column <= 7;
    }
}
