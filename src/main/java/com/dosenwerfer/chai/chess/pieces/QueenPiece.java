package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class QueenPiece extends Piece {

    public QueenPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public PieceType getType() {
        return PieceType.QUEEN;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        HashSet<Move> moves = new HashSet<>();

        // Rook & bishop moves
        RookPiece.addPossibleMoves( moves, position, square, color );
        BishopPiece.addPossibleMoves( moves, position, square, color );

        return moves;
    }
}
