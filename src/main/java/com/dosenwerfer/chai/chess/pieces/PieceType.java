package com.dosenwerfer.chai.chess.pieces;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
public enum PieceType {
    KING    ( 'K', '♔' /* \u2654 */, '♚' /* \u265A */ ),
    QUEEN   ( 'Q', '♕' /* \u2655 */, '♛' /* \u265B */ ),
    ROOK    ( 'R', '♖' /* \u2656 */, '♜' /* \u265C */ ),
    BISHOP  ( 'B', '♗' /* \u2657 */, '♝' /* \u265D */ ),
    KNIGHT  ( 'N', '♘' /* \u2658 */, '♞' /* \u265E */ ),
    PAWN    ( 'P', '♙' /* \u2659 */, '♟' /* \u265F */ );

    public final char token;
    public final char whiteSymbol;
    public final char blackSymbol;

    PieceType( char token, char whiteSymbol, char blackSymbol ) {
        this.token = token;
        this.whiteSymbol = whiteSymbol;
        this.blackSymbol = blackSymbol;
    }

    public char getSymbol( PieceColor color ) {
        return color == PieceColor.WHITE ? whiteSymbol : blackSymbol;
    }
}
