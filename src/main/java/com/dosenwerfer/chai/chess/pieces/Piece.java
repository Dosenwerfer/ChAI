package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import com.dosenwerfer.chai.chess.moves.TranslationMove;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
@EqualsAndHashCode
public abstract class Piece implements Cloneable {

    @Getter
    protected final PieceColor color;
    @Getter @Setter
    protected Square square;
    @Getter @Setter
    protected boolean moved = false;

    @Getter @EqualsAndHashCode.Exclude
    protected transient Board board;
    @Getter @EqualsAndHashCode.Exclude
    protected transient Player player;

    public Piece( Board board, Player player, Square square ) {
        this.board = board;
        this.player = player;
        this.color = player.getColor();
        this.square = square;
    }

    protected static HashSet<Move> addMoveIfInBoundsAndTargetNotOccupied( HashSet<Move> moves, Position position, Square to, Square from, PieceColor color ) {
        // Check if target square is out of bounds
        if ( !to.isInBounds() ) {
            return moves;
        }

        // Check if desired square is occupied by own piece
        if ( position.getBoard().isOccupied( to, color ) ) {
            return moves;
        }

        // Add move
        moves.add( new TranslationMove( color, from, to ) );

        return moves;
    }

    @Override
    public Piece clone() {
        Piece clonedPiece = null;
        try {
            clonedPiece = (Piece) super.clone();
        } catch ( CloneNotSupportedException e ) {
            e.printStackTrace();
        }
        return clonedPiece;
    }

    public char getSymbol() {
        return getType().getSymbol( color );
    }

    public String getToken() {
        return String.valueOf( color.getToken() ) + getType().token;
    }

    public abstract PieceType getType();

    public abstract HashSet<Move> getPossibleMoves( Position position );

    protected HashSet<Move> addMoveIfInBoundsAndTargetNotOccupied( HashSet<Move> moves, Position position, Square to ) {
        return addMoveIfInBoundsAndTargetNotOccupied( moves, position, to, square, color );
    }
}
