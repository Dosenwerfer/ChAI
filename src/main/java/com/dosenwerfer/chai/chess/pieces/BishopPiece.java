package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class BishopPiece extends Piece {

    public BishopPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public static HashSet<Move> addPossibleMoves( HashSet<Move> moves, Position position, Square origin, PieceColor color ) {
        final int row = origin.getRow();
        final int column = origin.getColumn();

        // Primary diagonal
        int primaryDiagonalLength = 8 - Math.abs( 7 - (row + column) );
        int primaryStartRow = row + column >= 7 ? 7 : row + column;
        int primaryStartColumn = row + column >= 7 ? column - (7 - row) : 0;
        for ( int delta = 0; delta < primaryDiagonalLength; ++delta ) {
            Square to = new Square( primaryStartRow - delta, primaryStartColumn + delta );
            addMoveIfInBoundsAndTargetNotOccupied( moves, position, to, origin, color );
        }

        // Secondary diagonal
        int secondaryDiagonalLength = 8 - Math.abs( row - column );
        int secondaryStartRow = row > column ? row - column : 0;
        int secondaryStartColumn = row > column ? 0 : column - row;
        for ( int delta = 0; delta < secondaryDiagonalLength; ++delta ) {
            Square to = new Square( secondaryStartRow + delta, secondaryStartColumn + delta );
            addMoveIfInBoundsAndTargetNotOccupied( moves, position, to, origin, color );
        }

        return moves;
    }

    public PieceType getType() {
        return PieceType.BISHOP;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        return addPossibleMoves( new HashSet<>(), position, square, color );
    }
}
