package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class KnightPiece extends Piece {

    public KnightPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public static HashSet<Move> addPossibleMoves( HashSet<Move> moves, Position position, Square origin, PieceColor color ) {
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() + 1, origin.getColumn() + 2 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() + 2, origin.getColumn() + 1 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() + 2, origin.getColumn() - 1 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() + 1, origin.getColumn() - 2 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() - 1, origin.getColumn() - 2 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() - 2, origin.getColumn() - 1 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() - 2, origin.getColumn() + 1 ), origin, color );
        addMoveIfInBoundsAndTargetNotOccupied( moves, position, new Square( origin.getRow() - 1, origin.getColumn() + 2 ), origin, color );

        return moves;
    }

    public PieceType getType() {
        return PieceType.KNIGHT;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        return addPossibleMoves( new HashSet<>(), position, square, color );
    }
}
