package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class RookPiece extends Piece {

    public RookPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public static HashSet<Move> addPossibleMoves( HashSet<Move> moves, Position position, Square origin, PieceColor color ) {
        // Vertical
        for ( int row = 0; row <= 7; ++row ) {
            Square to = new Square( row, origin.getColumn() );
            addMoveIfInBoundsAndTargetNotOccupied( moves, position, to, origin, color );
        }
        // Horizontal
        for ( int column = 0; column <= 7; ++column ) {
            Square to = new Square( origin.getRow(), column );
            addMoveIfInBoundsAndTargetNotOccupied( moves, position, to, origin, color );
        }

        return moves;
    }

    public PieceType getType() {
        return PieceType.ROOK;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        return addPossibleMoves( new HashSet<>(), position, square, color );
    }

    public enum RookType {
        SHORT, LONG
    }
}
