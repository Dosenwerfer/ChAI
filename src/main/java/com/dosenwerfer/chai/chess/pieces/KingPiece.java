package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.Move;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class KingPiece extends Piece {

    public KingPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public PieceType getType() {
        return PieceType.KING;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        HashSet<Move> moves = new HashSet<>();

        // Regular moves
        for ( int row = -1; row <= 2; ++row ) {
            for ( int column = -1; column <= 2; ++column ) {
                if ( row == 0 && column == 0 ) {
                    continue;
                }
                Square to = new Square( square.getRow() + row, square.getColumn() + column );

                // Check if target is attacked
                if ( position.getPlayers().get( color ).isAttacked( to ) ) {
                    continue;
                }

                // Add if valid
                addMoveIfInBoundsAndTargetNotOccupied( moves, position, to );
            }
        }

        return moves;
    }
}
