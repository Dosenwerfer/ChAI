package com.dosenwerfer.chai.chess.pieces;

import lombok.Getter;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
public enum PieceColor {
    WHITE('W'),
    BLACK('B');

    @Getter
    private final char token;

    PieceColor( char token ) {
        this.token = token;
    }

    public PieceColor invert() {
        return this == WHITE ? BLACK : WHITE;
    }
}
