package com.dosenwerfer.chai.chess.pieces;

import com.dosenwerfer.chai.chess.Player;
import com.dosenwerfer.chai.chess.Position;
import com.dosenwerfer.chai.chess.board.Board;
import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.HeadStartTranslationMove;
import com.dosenwerfer.chai.chess.moves.Move;
import com.dosenwerfer.chai.chess.moves.PromotionTranslationMove;
import com.dosenwerfer.chai.chess.moves.TranslationMove;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;

/**
 * @author Dosenwerfer (16-Jan-19).
 */
@EqualsAndHashCode(callSuper = true)
public class PawnPiece extends Piece {

    @Getter @Setter
    private boolean justHeadStarted = false;

    public PawnPiece( Board board, Player player, Square square ) {
        super( board, player, square );
    }

    public PieceType getType() {
        return PieceType.PAWN;
    }

    public HashSet<Move> getPossibleMoves( Position position ) {
        HashSet<Move> moves = new HashSet<>();
        Board board = position.getBoard();

        // Pawn moves
        // Head start:
        // Check if desired square is not occupied by any piece (may not capture through this move)
        // Check if square that we are stepping over is not occupied
        if ( color == PieceColor.WHITE ) {
            if ( square.getRow() == 1 ) {
                Square to = new Square( 3, square.getColumn() );
                if ( to.isInBounds() && !board.isOccupied( to ) && !board.isOccupied( new Square( 2, square.getColumn() ) ) ) {
                    // Add move
                    moves.add( new HeadStartTranslationMove( color, square, to ) );
                }
            }
        } else {
            if ( square.getRow() == 6 ) {
                Square to = new Square( 4, square.getColumn() );
                if ( to.isInBounds() && !board.isOccupied( to ) && !board.isOccupied( new Square( 5, square.getColumn() ) ) ) {
                    // Add move
                    moves.add( new HeadStartTranslationMove( color, square, to ) );
                }
            }
        }

        // Standard moves
        {
            Square to = new Square( square.getRow() + (color == PieceColor.WHITE ? 1 : -1), square.getColumn() );
            if ( to.isInBounds() && !board.isOccupied( to ) ) {
                // Check if move is promotion move
                if ( to.getRow() == (color == PieceColor.WHITE ? 7 : 0) ) {
                    // Add promotion move
                    PromotionTranslationMove.addPromotionMoves( moves, player, position, square, to );
                } else {
                    // Add regular move
                    moves.add( new TranslationMove( color, square, to ) );
                }
            }
        }

        // Capture
        if ( color == PieceColor.WHITE ) {
            // Left
            Square to = new Square( square.getRow() + 1, square.getColumn() - 1 );
            if ( to.isInBounds() && board.isOccupied( to, PieceColor.BLACK ) ) {
                // Add move
                moves.add( new TranslationMove( color, square, to ) );
            }
            // Right
            to = new Square( square.getRow() + 1, square.getColumn() - 1 );
            if ( to.isInBounds() && board.isOccupied( to, PieceColor.BLACK ) ) {
                // Add move
                moves.add( new TranslationMove( color, square, to ) );
            }
        } else {
            // Left
            Square to = new Square( square.getRow() - 1, square.getColumn() - 1 );
            if ( to.isInBounds() && board.isOccupied( to, PieceColor.WHITE ) ) {
                // Add move
                moves.add( new TranslationMove( color, square, to ) );
            }
            // Right
            to = new Square( square.getRow() - 1, square.getColumn() - 1 );
            if ( to.isInBounds() && board.isOccupied( to, PieceColor.WHITE ) ) {
                // Add move
                moves.add( new TranslationMove( color, square, to ) );
            }
        }

        // En passant
        // Check if own pawn is on according row
        if ( color == PieceColor.WHITE && square.getRow() == 3
                || color == PieceColor.BLACK && square.getRow() == 4 ) {
            // Check if there's an enemy pawn to the left
            Piece left = board.getPiece( new Square( square.getRow(), square.getColumn() - 1 ) );
            if ( left instanceof PawnPiece
                    && left.getColor() == color.invert()
                    && ((PawnPiece) left).isJustHeadStarted() ) {
                moves.add( new TranslationMove( color, square, new Square(
                                square.getRow() + ( color == PieceColor.WHITE ? 1 : -1 ),
                                square.getColumn() - 1 ) ) );
            }
            // Check if there's an enemy pawn to the right
            Piece right = board.getPiece( new Square( square.getRow(), square.getColumn() + 1 ) );
            if ( right instanceof PawnPiece
                    && right.getColor() == color.invert()
                    && ((PawnPiece) right).isJustHeadStarted() ) {
                moves.add( new TranslationMove( color, square, new Square(
                        square.getRow() + ( color == PieceColor.WHITE ? 1 : -1 ),
                        square.getColumn() + 1 ) ) );
            }
        }

        return moves;
    }
}
