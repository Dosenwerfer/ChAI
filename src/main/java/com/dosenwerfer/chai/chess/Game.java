package com.dosenwerfer.chai.chess;

import com.dosenwerfer.chai.chess.moves.Move;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author Dosenwerfer (05-Sep-18).
 */
public class Game {

    @Getter
    private Position currentPosition;
    private final HashMap<Position, Byte> positionOccurrences = new HashMap<>();
    private boolean finished;
    @Getter @Setter
    private boolean useSymbols;

    public Game() {
        this.currentPosition = new Position( this );
        this.finished = false;
        this.useSymbols = true;
    }

    public void start( BufferedReader reader ) {
        System.out.println( "A new game has started!" );
        finished = false;
        try {
            while ( !finished ) {
                System.out.println( "Current position:" );
                if ( useSymbols ) {
                    currentPosition.getBoard().printSymbols();
                } else {
                    currentPosition.getBoard().print();
                }
                System.out.println( "Make your move " + currentPosition.getTurn().name() + "! Syntax:" );
                System.out.println( "<from> <to> [promotion-piece] (e.g. \"a2 a4\" or \"a7 a8 Q\")" );
                System.out.println( "<quit/q/abort/a> or <symbols> (toggles between output format)" );
                System.out.print( ">" );
                String input = reader.readLine();
                // Catch control inputs and default to move evaluation
                switch ( input ) {
                    case "a":
                    case "q":
                    case "abort":
                    case "quit":
                        finished = true;
                        return;
                    default:
                        Move move = evaluateMoveString( input );
                        if ( move == null ) {
                            // TODO error message if wrong input
                        } else {
                            // TODO do something with entered move
                        }
                }
            }
        } catch ( IOException ex ) {
            ex.printStackTrace();
        } finally {
            finished = true;
        }
    }

    /**
     * Returns the number of occurrences of a currentPosition.
     *
     * @param position Position
     * @return Number of occurrences of the Position
     */
    public byte getPositionOccurrences( Position position ) {
        Byte result = positionOccurrences.get( position );
        return result == null ? 0 : result;
    }

    /**
     * Adds a currentPosition occurrence.
     *
     * @param position Position
     */
    public void addPositionOccurrence( Position position ) {
        // Increment and save
        positionOccurrences.put( position, (byte) (getPositionOccurrences( position ) + (byte) 1) );
    }

    /**
     *
     * @param input
     * @return Intended <code>Move</code> or <code>null</code> if Move is not valid
     */
    private Move evaluateMoveString( String input ) {
        // TODO evaluate move string using PGNParser
        return null;
    }
}
