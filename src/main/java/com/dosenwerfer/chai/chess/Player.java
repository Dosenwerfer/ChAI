package com.dosenwerfer.chai.chess;

import com.dosenwerfer.chai.chess.board.Square;
import com.dosenwerfer.chai.chess.moves.CastlingMove;
import com.dosenwerfer.chai.chess.moves.Move;
import com.dosenwerfer.chai.chess.moves.OutcomeMove;
import com.dosenwerfer.chai.chess.moves.TranslationMove;
import com.dosenwerfer.chai.chess.pieces.KingPiece;
import com.dosenwerfer.chai.chess.pieces.Piece;
import com.dosenwerfer.chai.chess.pieces.PieceColor;
import com.dosenwerfer.chai.chess.pieces.RookPiece;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TODO docs
 * Stateful color class
 *
 * @author Dosenwerfer (22-Feb-19).
 */
@EqualsAndHashCode
public class Player implements Cloneable {

    @Getter
    private final PieceColor color;

    @Getter @EqualsAndHashCode.Include
    private transient HashMap<Boolean, Set<Move>> moves = new HashMap<>();

    @Getter @Setter
    private transient Position position;
    @Getter @Setter
    private transient KingPiece king;
    @Getter
    private transient HashMap<RookPiece.RookType, RookPiece> rooks = new HashMap<>();
    private transient HashMap<Square, Boolean> attackedSquaresCache = new HashMap<>();

    public Player( PieceColor color, Position position ) {
        this.color = color;
        this.position = position;
    }

    /**
     * Computes this color's legal moves.
     */
    public void computeMoves() {
        moves.put( false, determineLegalMoves( false ) );
        moves.put( true, determineLegalMoves( true ) );
    }

    /**
     * <p>Collects all currently legal moves of a color by looking at each of their pieces
     * and their respecting legal moves and eliminating illegal moves implied by higher chess rules
     * by considering check, checkmate, stalemate and castling states.</p>
     *
     * <p>Serves as a singleton-like cache, that only determines the legal moves once
     * and then continues by returning that result on future method calls.</p>
     *
     * @param includeEnemyDependentMoves If moves, that would need enemy moves should be included (needed to prevent infinite lööp)
     * @return Set of legal moves for the specified color
     */
    private Set<Move> determineLegalMoves( boolean includeEnemyDependentMoves ) {
        Set<Move> legalMoves = new HashSet<>();
        // Get all moves
        for ( Piece piece : position.getBoard().getPieceSet( color ) ) {
            // Regular moves
            legalMoves.addAll( piece.getPossibleMoves( position ) );

            // Castling moves
            if ( includeEnemyDependentMoves && piece instanceof KingPiece ) {
                for ( CastlingMove.CastlingType type : CastlingMove.CastlingType.values() ) {
                    if ( canCastle( type ) ) {
                        legalMoves.add( new CastlingMove( color, type ) );
                    }
                }
            }
        }

        // Check for stalemate
        if ( legalMoves.isEmpty() ) {
            // Stalemate is present
            // Add OutcomeMove as only option
            legalMoves.add( new OutcomeMove( OutcomeMove.Outcome.DRAW, color ) );
        } else {
            // Check for check
            KingPiece king = position.getBoard().getKing( color );
            if ( includeEnemyDependentMoves && king != null && position.getPlayers().get( color ).isAttacked( king.getSquare() ) ) {
                // Player is in check
                // Only keep moves, that break the check
                legalMoves = legalMoves.stream().filter( move -> {
                    Position next = move.apply( position );
                    KingPiece newKing = next.getBoard().getKing( color );
                    return !next.getPlayers().get( color ).isAttacked( newKing.getSquare() );
                } ).collect( Collectors.toSet() );
                // Check if there are legalMoves remaining
                if ( legalMoves.isEmpty() ) {
                    // TODO Checkmate handling
                }
            }
        }

        return legalMoves;
    }

    /**
     * Determines if this color has castling rights.
     *
     * @param type Castling type
     * @return if the color has the castling right for the specified type
     */
    public boolean canCastle( CastlingMove.CastlingType type ) {
        // Get involved pieces
        RookPiece rook = rooks.get( type.getRookType() );

        //// Check if involved pieces have moved yet ////
        if ( king.isMoved() || rook.isMoved() ) {
            return false;
        }

        //// Check if squares are safe for the king to move across ////
        Square target = CastlingMove.getSquare( color, type, null, CastlingMove.Direction.TO );

        // Ensure that start column is smaller than end column
        Square start = king.getSquare();
        if ( start.getColumn() > target.getColumn() ) {
            Square tmp = start;
            start = target;
            target = tmp;
        }

        // Move king test-wise to all crossed squares
        for ( int column = start.getColumn(); column <= target.getColumn(); ++column ) {
            Square square = new Square( start.getRow(), column );

            // Check if square is occupied
            if ( position.getBoard().isOccupied( square ) ) {
                return false;
            }

            // Move king test-wise
            Position mock = new TranslationMove( color, start, square ).apply( position );

            // Check if square would be under attack by opponent in mock position
            if ( mock.getPlayers().get( king.getColor().invert() ).isAttacked( square ) ) {
                return false;
            }
        }

        //// All requirements have been met ////
        return true;
    }


    /**
     * Checks if a square is under attack by opponent's pieces.
     *
     * This method uses a cache, since execution is expensive and also deterministic.
     *
     * @param square Square that should be checked
     * @return If square is under attack
     */
    public boolean isAttacked( Square square ) {
        //// Check if result has been cached ////
        Boolean attacked = attackedSquaresCache.get( square );
        if ( attacked != null ) {
            return attacked;
        }

        //// Determine result /////
        Set<Move> enemyMoves = this.position.getPlayers().get( color.invert() ).moves.get( false );
        attacked = enemyMoves != null && enemyMoves.stream().anyMatch( move ->
                move instanceof TranslationMove && Objects.equals( ( (TranslationMove) move ).getTo(), square ) );

        //// Cache result ////
        this.attackedSquaresCache.put( square, attacked );

        //// Return result ////
        return attacked;
    }

    /**
     * Warning: position, moves and attacked squares won't be cloned,
     * since they are transient and depend on game state.
     *
     * @return Cloned Player
     */
    @Override
    public Player clone() {
        Player clonedPlayer = null;
        try {
            // Call super
            clonedPlayer = (Player) super.clone();
        } catch ( CloneNotSupportedException e ) {
            e.printStackTrace();
        }
        return clonedPlayer;
    }
}
